define (require)->

  config = require '../config'
    
  class UserModel extends Backbone.Model
    url: =>
      "#{config.restPath}/api/2/user?username=#{@id}"
    initialize: (options) ->
      @fetch(options).done (d) =>
        options.callback d if options.callback