define (require)->

  config = require '../config'

  class CustomFieldsCollection extends Backbone.Collection
    url: "#{config.restPath}/api/2/field"
    initialize: ->
      @fetch().done @setFields
    setFields: =>
      _.each config.customFields, (cf) =>
        field = @find (f) =>
          f.attributes.name == cf.name
        _.extend cf, field.attributes