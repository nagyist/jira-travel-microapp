define (require)->

  config = require '../config'
    
  class UsersCollection extends Backbone.Collection
    url: (opts) =>
      "#{config.restPath}/api/2/user/search?username=#{@q}"
    initialize: (options) ->
      @q = options.q
      @fetch(options).done (d) =>
        options.callback(d) if options.callback

