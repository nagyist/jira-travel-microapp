define ->
  
  class PendingTripsView extends Backbone.View
    el: $ '#pendingtrips-rows'

    template: _.template $('#pendingtrips-template').html()

    initialize: (options)->
      @render()

    events:
      "click .complete-booking": "completeBooking"

    completeBooking: (e) ->
      el = $(e.currentTarget)
      trip = el.data()
      $.get "#{config.restPath}/api/2/issue/#{trip.key}/transitions", (d) =>
        bookTransition = _.find d.transitions, (t) =>
          t.name == config.bookTransitionName
        $.ajax
          url: "#{config.restPath}/api/2/issue/#{trip.key}/transitions"
          type: "post"
          dataType: "json"
          contentType: "application/json"
          processData: false
          data:
            JSON.stringify
              transition:
                id: bookTransition.id
          success: =>
            el.removeClass('btn-success').addClass('btn-inverse').addClass('disabled').html('Booked')
            @collection.get(trip.id).set('status', {name: "Booked"}, {silent: true})
            @collection.trigger 'change'
      false

    render: ->
      @collection.filter (d) ->
        d.attributes.status.name == "Approved"
      .sort (a,b) ->
        a.get(config.customFields.departDate.id) > b.get(config.customFields.returnDate.id)
      .each (model) =>
        @$el.append @template(model)
